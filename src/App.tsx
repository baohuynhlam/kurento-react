import React, { useEffect } from "react";
import kurentoClient from "kurento-client";
import adapter from "webrtc-adapter";

function App() {
    useEffect(() => {
        const servers = {
            iceServers: [
                {
                    urls: [
                        "stun:stun1.l.google.com:19302",
                        "stun:stun2.l.google.com:19302",
                    ],
                },
            ],
        };

        const videoInput = document.getElementById(
            "videoInput"
        )! as HTMLVideoElement;
        const videoOutput = document.getElementById(
            "videoOutput"
        )! as HTMLVideoElement;
        const startButton = document.getElementById(
            "start"
        )! as HTMLButtonElement;
        const stopButton = document.getElementById(
            "stop"
        )! as HTMLButtonElement;
        const playButton = document.getElementById(
            "play"
        )! as HTMLButtonElement;

        startButton.onclick = async () => {
            const pc1 = new RTCPeerConnection(servers);
            let localStream: MediaStream;
            // @ts-ignore
            localStream = await navigator.mediaDevices.getDisplayMedia({
                video: true,
                audio: true,
            });
            const audioStream = await navigator.mediaDevices.getUserMedia({
                audio: true,
            });

            audioStream.getTracks().forEach((audioTrack) => {
                localStream.addTrack(audioTrack);
            });

            // Push tracks from local stream to peer connection
            localStream.getTracks().forEach((track) => {
                console.log("pc1 addTrack", track);
                pc1.addTrack(track, localStream);
            });

            // Show stream in HTML video
            videoInput.srcObject = localStream;

            try {
                // Make an offer
                const offerDescription = await pc1.createOffer();

                // Connect to Kurento Client
                const client = await kurentoClient(
                    "ws://localhost:8888/kurento"
                );
                console.log("Connected to Kurento");

                const pipeline = await client.create("MediaPipeline");

                const webRTCendpoint = await pipeline.create("WebRtcEndpoint");

                pc1.onicecandidate = (event) => {
                    console.log("pc1 oninecandidate");
                    if (event.candidate) {
                        const complexCandidate = kurentoClient.getComplexType(
                            "IceCandidate"
                        )(event.candidate);
                        webRTCendpoint.addIceCandidate(complexCandidate);
                    }
                };

                webRTCendpoint.on("IceCandidateFound", (event) => {
                    console.log("pc1 IceCandidateFound");
                    event.candidate && pc1.addIceCandidate(event.candidate);
                });

                // Process offer
                await pc1.setLocalDescription(offerDescription);
                const answerSDP = await webRTCendpoint.processOffer(
                    offerDescription.sdp
                );
                await pc1.setRemoteDescription({
                    sdp: answerSDP,
                    type: "answer",
                });
                await webRTCendpoint.gatherCandidates();

                // Connect the Kurento elements
                const recorder = await pipeline.create("RecorderEndpoint", {
                    uri: "file:///tmp/demo2.mp4",
                    stopOnEndOfStream: true,
                });
                await webRTCendpoint.connect(recorder);

                stopButton.onclick = () => {
                    recorder.stop();
                    pipeline.release();
                    pc1.close();
                    localStream.getTracks().forEach((track) => {
                        track.stop();
                    });
                    videoInput.srcObject = null;
                };

                await recorder.record();

                console.log("Record");
            } catch (error) {
                console.log("pc1 error", error);
                localStream.getTracks().forEach((track) => {
                    track.stop();
                });
                videoInput.srcObject = null;
            }
        };

        playButton.onclick = async () => {
            const pc2 = new RTCPeerConnection(servers);

            // Pull tracks from remote stream, add to video stream
            let remoteStream = new MediaStream();
            pc2.ontrack = (event) => {
                console.log("pc2 ontrack");
                remoteStream.addTrack(event.track);
            };
            videoOutput.srcObject = remoteStream;

            pc2.addTransceiver("audio", {
                direction: "recvonly",
            });

            pc2.addTransceiver("video", {
                direction: "recvonly",
            });

            try {
                const client = await kurentoClient(
                    "ws://localhost:8888/kurento"
                );
                console.log("Connected to Kurento 2nd time");

                const offerDescription = await pc2.createOffer();

                const pipeline = await client.create("MediaPipeline");

                const rtcEp = await pipeline.create("WebRtcEndpoint");

                await rtcEp.setMinVideoSendBandwidth(2000);

                pc2.onicecandidate = (event) => {
                    console.log("pc2 onicecandidate");
                    if (event.candidate) {
                        const complexCandidate = kurentoClient.getComplexType(
                            "IceCandidate"
                        )(event.candidate);
                        rtcEp.addIceCandidate(complexCandidate);
                    }
                };

                rtcEp.on("IceCandidateFound", (event) => {
                    console.log("pc2 IceCandidateFound");
                    event.candidate && pc2.addIceCandidate(event.candidate);
                });

                // Process offer
                await pc2.setLocalDescription(offerDescription);
                console.log("set a local description");
                const answerSDP = await rtcEp.processOffer(
                    offerDescription.sdp
                );
                await pc2.setRemoteDescription({
                    sdp: answerSDP,
                    type: "answer",
                });

                await rtcEp.gatherCandidates();

                const player = await pipeline.create("PlayerEndpoint", {
                    uri: "file:///tmp/demo2.mp4",
                });

                player.on("EndOfStream", () => {
                    console.log("end of stream");
                    remoteStream.getTracks().forEach((track) => {
                        track.stop();
                    });
                    videoOutput.srcObject = null;
                    pipeline.release();
                    pc2.close();
                });

                await player.connect(rtcEp);
                console.log("Connected player and webRTCendpoint");

                player.play();
                console.log("Playing");

                pc2.getReceivers().forEach((r) => {
                    pc2.addTrack(r.track);
                });
            } catch (err) {
                console.log("Pc2 error", err);
            }
        };
    });

    return (
        <React.Fragment>
            <head>
                <meta charSet="utf-8" />
                <meta http-equiv="cache-control" content="no-cache" />
                <meta http-equiv="pragma" content="no-cache" />
                <meta http-equiv="expires" content="0" />
                <meta
                    name="viewport"
                    content="width=device-width, initial-scale=1.0"
                />

                <link
                    rel="stylesheet"
                    href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
                    integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
                    crossOrigin="anonymous"
                />

                <script
                    src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
                    integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
                    crossOrigin="anonymous"
                ></script>
                <script
                    src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
                    integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
                    crossOrigin="anonymous"
                ></script>
                <script
                    src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
                    integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
                    crossOrigin="anonymous"
                ></script>

                <script
                    src="https://cdnjs.cloudflare.com/ajax/libs/kurento-client/6.16.0/js/kurento-client.min.js"
                    integrity="sha512-eSbtixs7l1ONKNSqbN5T7DJuqQwOVoEUM1s4zdPAsbgPgl4pPvMRiHnTKVq+iRBTeRUW+lALL33lNpjWH5ROJg=="
                    crossOrigin="anonymous"
                ></script>
                <title>Kurento Tutorial: Recorder</title>
            </head>
            <body>
                <div className="container">
                    <div className="page-header">
                        <h1>Tutorial: Recorder</h1>
                        <p>
                            This application shows a <i>WebRtcEndpoint</i>{" "}
                            connected to itself (loopback) with a{" "}
                            <i>RecorderEndpoint</i>. Take a look to the
                            <a
                                data-toggle="lightbox"
                                data-title="Recorder Pipeline"
                                data-footer="WebRtcEnpoint in loopback with RecorderEndpoint"
                            >
                                Media Pipeline
                            </a>
                            . To run this demo follow these steps:
                        </p>
                        <ol>
                            <li>
                                Open this page with a browser compliant with
                                WebRTC (Chrome, Firefox).
                            </li>
                            <li>
                                Click on <i>Start</i> button.
                            </li>
                            <li>
                                Grant the access to the camera and microphone.
                                After the SDP negotiation the loopback should
                                start.
                            </li>
                            <li>
                                Click on <i>Stop</i> to finish the
                                communication.
                            </li>
                            <li>
                                Click on <i>Play</i> to see the recording.
                            </li>
                        </ol>
                    </div>
                    <div className="row">
                        <div className="col-md-5">
                            <h3>Local stream</h3>
                            <video
                                id="videoInput"
                                autoPlay
                                width="480px"
                                height="360px"
                            ></video>
                        </div>
                        <div className="col-md-2">
                            <button id="start" className="btn btn-success">
                                Start
                            </button>
                            <br />
                            <br />
                            <button id="stop" className="btn btn-danger">
                                Stop
                            </button>
                            <br />
                            <br />
                            <button id="play" className="btn btn-warning">
                                Play
                            </button>
                        </div>
                        <div className="col-md-5">
                            <h3>Remote stream</h3>
                            <video
                                id="videoOutput"
                                autoPlay
                                width="480px"
                                height="360px"
                            ></video>
                        </div>
                    </div>
                </div>
            </body>
        </React.Fragment>
    );
}

export default App;
